#!/usr/local/bin/python
# export.py <port> <database>

from settings import EXPORT
from settings.ENVIRONMENTS import get_environment
from subprocess import call
from datetime import datetime
import sys
import os


# error handling
def display_arguments():
    print("python export.py <environment>")


def check_arguments():
    if len(sys.argv) < 2:
        print("You must provide the environment name.")
        display_arguments()
        sys.exit(0)

check_arguments()

ENV = get_environment()
collections = EXPORT['collections']

print(repr(ENV))
uri = ENV['database_uri']
parts = uri.replace('mongodb://', '').split('/')
host_parts = parts[0].split(':')
host = host_parts[0]
port = host_parts[1]
database = parts[1]
print(host, port)

timestamp = datetime.now().strftime('%Y%m%d%H%M%S')
folder = './exports/{}'.format(str(timestamp))

os.system('mkdir -p {}'.format(folder))
for collection in collections:
    print(repr(collection))
    command = "mongoexport --host {} --port {} --db {} --collection {} > {}".format(
        host,
        port,
        database,
        collection,
        '{}/{}.json'.format(folder, collection)
    )
    print(repr(command))
    os.system(command)

os.system('cd exports;tar -czf ./export-{}.tar.gz {};cd ..'.format(str(timestamp), str(timestamp)))
os.system('rm -rf {}'.format(folder))
