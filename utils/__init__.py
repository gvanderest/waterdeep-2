from random import randint
import hashlib

CSI = chr(27) + '['
BRACKET_PLACEHOLDER = CSI + 'XXX'


def parse_colors(in_string):
    """ Convert a MUD color code to ANSI color """

    if not in_string:
        return ''

    out_string = in_string

    # make the { symbol possible
    out_string = out_string.replace("{{", BRACKET_PLACEHOLDER)

    # reset
    out_string = out_string.replace("{9", CSI + '0m')
    out_string = out_string.replace("{x", CSI + '0m')

    # red
    out_string = out_string.replace("{!", CSI + '1;31m')
    out_string = out_string.replace("{R", CSI + '1;31m')
    out_string = out_string.replace("{1", CSI + '0;31m')
    out_string = out_string.replace("{r", CSI + '0;31m')

    # green
    out_string = out_string.replace("{@", CSI + '1;32m')
    out_string = out_string.replace("{G", CSI + '1;32m')
    out_string = out_string.replace("{2", CSI + '0;32m')
    out_string = out_string.replace("{g", CSI + '0;32m')

    # yellow
    out_string = out_string.replace("{#", CSI + '1;33m')
    out_string = out_string.replace("{Y", CSI + '1;33m')
    out_string = out_string.replace("{3", CSI + '1;33m')
    out_string = out_string.replace("{y", CSI + '0;33m')

    # blue
    out_string = out_string.replace("{$", CSI + '1;34m')
    out_string = out_string.replace("{B", CSI + '1;34m')
    out_string = out_string.replace("{4", CSI + '0;34m')
    out_string = out_string.replace("{b", CSI + '0;34m')

    # magenta
    out_string = out_string.replace("{%", CSI + '1;35m')
    out_string = out_string.replace("{M", CSI + '1;35m')
    out_string = out_string.replace("{5", CSI + '0;35m')
    out_string = out_string.replace("{m", CSI + '0;35m')

    # cyan
    out_string = out_string.replace("{^", CSI + '1;36m')
    out_string = out_string.replace("{C", CSI + '1;36m')
    out_string = out_string.replace("{6", CSI + '0;36m')
    out_string = out_string.replace("{c", CSI + '0;36m')

    # white
    out_string = out_string.replace("{&", CSI + '1;37m')
    out_string = out_string.replace("{W", CSI + '1;37m')
    out_string = out_string.replace("{7", CSI + '0;37m')
    out_string = out_string.replace("{w", CSI + '0;37m')

    # dark grey
    out_string = out_string.replace("{8", CSI + '1;30m')
    out_string = out_string.replace("{*", CSI + '1;30m')

    # random color
    while out_string.find('{-') > -1:
        highlight = randint(0, 1)
        if highlight == 1:
            color = randint(30, 37)
        else:
            color = randint(31, 37)

        out_string = out_string.replace("{-", CSI + str(highlight) + ';' + str(color) + 'm', 1)

    out_string = out_string.replace(BRACKET_PLACEHOLDER, '{')

    return out_string


def get_proper_case(in_string):
    parts = in_string.split(' ')
    outputs = []
    for word in parts[:]:
        word = word.lower()
        outputs.append(word[:1].upper() + word[1:])

    return ' '.join(outputs)


def get_password_hash(password):
    m = hashlib.sha1(password.strip())
    return m.hexdigest()


def get_colored_race_short_name(race):
    short_name = race['short_name']

    primary_color = '{C'
    secondary_color = '{c'

    return primary_color + short_name[0:1] + secondary_color + short_name[1:] + '{x'


def get_colored_class_short_name(c):
    short_name = c['short_name']

    primary_color = '{R'
    secondary_color = '{r'

    if c['tier'] == 2:
        primary_color = '{B'
        secondary_color = '{b'

    return primary_color + short_name[0:1] + secondary_color + short_name[1:] + '{x'

