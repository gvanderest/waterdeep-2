#!/usr/local/bin/python

from settings import EXPORT
from settings.ENVIRONMENTS import get_environment
import sys
import os


# error handling
def display_arguments():
    print("python import.py <timestamp> <environment>")


def check_arguments():
    if len(sys.argv) < 2:
        print("You must provide the timestamp of the export you wish to import.")
        display_arguments()
        sys.exit(0)
    elif len(sys.argv) < 3:
        print("You must provide the environment name.")
        display_arguments()
        sys.exit(0)

check_arguments()

timestamp = sys.argv[1]
ENV = get_environment(sys.argv[2])
collections = EXPORT['collections']

uri = ENV['database_uri']
parts = uri.replace('mongodb://', '').split('/')
host_parts = parts[0].split(':')
host = host_parts[0]
port = host_parts[1]
database = parts[1]

filename = "export-{}.tar.gz".format(timestamp)
folder = './exports'
input_filename = '{}/{}'.format(folder, filename)
temp_folder = '{}/{}'.format(folder, timestamp)

if not os.path.isfile(input_filename):
    print("The export '{}' does not exist, please ensure '{}' is in the '{}' folder".format(
        timestamp,
        filename,
        folder
    ))
    sys.exit(0)

os.system('cd {};tar zxvf {};cd ..'.format(
    folder,
    filename
))

for collection in collections:
    input_file = '{}/{}.json'.format(temp_folder, collection)
    print(repr(collection))
    command = "mongoimport --host {} --port {} --db {} --collection {} --drop < {}".format(
        host,
        port,
        database,
        collection,
        input_file
    )
    print(repr(command))
    os.system(command)

os.system('rm -rf {}'.format(temp_folder))
