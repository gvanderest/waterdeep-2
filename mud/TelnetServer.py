from settings import DELAYS
from TelnetConnection import TelnetConnection
import time
import socket
import thread


class TelnetServer(object):
    def __init__(self, address, port):
        self.address = address
        self.port = port
        self.connections = []

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((self.address, self.port))
        self.socket.listen(0)

        self.index = -1

        self.listening = True

        thread.start_new_thread(self.handle_flushings, ())

    def handle_flushings(self):
        """ Handle the flushing of all socket contents """
        # TODO handle goign linkdead from no communication
        while self.listening:
            for connection in self.connections:
                connection.flush()
                # FIXME use a constant
            time.sleep(DELAYS['socket_flush'])

    def handle_connections(self):
        while self.listening:
            conn, addr = self.socket.accept()
            thread.start_new_thread(self.handle_connection, (conn, addr))

    def handle_connection(self, connection, address, quit=False):
        self.index += 1
        connection = TelnetConnection(index=self.index, server=self, socket=connection, address=address)
        self.connections.append(connection)
        connection.listen()

        # if this line is reached, the listen method failed
        # TODO: add linkdead code?
        print('LINKDEAD ISSUE')
        actor = connection.actor
        if actor:
            # FIXME handle quitting versus linkdeadding
            actor.broadcast_to_room('disconnect')
        self.destroy_connection(connection)

    def destroy_connection(self, connection, quit=False):
        if connection in self.connections:
            del self.connections[self.connections.index(connection)]
        try:
            connection.socket.shutdown(socket.SHUT_RDWR)
            connection.socket.close()
        except Exception:
            print(repr("** EXCEPTION WHILE CLOSING CONNECTION, SEMI-EXPECTED"))
            pass

    def broadcast(self, message):
        for connection in self.connections:
            connection.writeln(message)
