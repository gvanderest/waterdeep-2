import sys
import thread
import time
from settings.ENVIRONMENTS import get_environment
from mud.TelnetServer import TelnetServer


class MUD(object):
    """ Multi-User Dungeon """
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(MUD, cls).__new__(cls, *args, **kwargs)

        return cls._instance

    def __init__(self):
        self.running = True

    def run(self):
        # the connection handlers
        self.servers = []

        # telnet
        env = get_environment()
        telnet_server = TelnetServer(address='', port=env['port'])
        self.servers.append(telnet_server)
        thread.start_new_thread(telnet_server.handle_connections, ())

        while self.running:
            time.sleep(1)

    def broadcast(self, message):
        for server in self.servers:
            server.broadcast(message)

    def get_connections(self):
        output = []
        for server in self.servers:
            for connection in server.connections:
                if connection.connected:
                    output.append(connection)

        return output

    def get_connections_by_actor(self, actor):
        output = []

        connections = self.get_connections()
        for connection in connections:
            if connection.actor and connection.actor._id == actor._id:
                output.append(connection)

        return output

    def get_online_characters(self, room_vnum=None):
        connections = self.get_connections()
        output = []
        for connection in connections:
            actor = connection.actor
            if connection.actor:
                if room_vnum is not None:
                    actor.fetch(fields=['room_vnum'])
                    if actor.room_vnum == room_vnum:
                        output.append(actor.clone())
                else:
                    output.append(actor.clone())

        return output
