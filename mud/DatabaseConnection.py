from settings.ENVIRONMENTS import get_environment
from pymongo import MongoClient
from pymongo.database import Database


class DatabaseConnection(Database):
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            env = get_environment()
            conn = MongoClient(env['database_uri'])
            database = conn.get_default_database()
            cls._instance = database

        return cls._instance
