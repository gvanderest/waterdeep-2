#from mud.LoginState import LoginState
from settings import DIRECTIONS, DELAYS
from models import Actor, Character
import socket
import thread
import time
import utils


class TelnetConnection(object):
    def __init__(self, index, server, socket, address):
        self.index = index
        self.server = server
        self.socket = socket
        self.address = address
        self.state = 'login'
        self.substate = 'username'
        self.actor = None
        self.connected = True
        self.buffer = ''

        self.input_buffer = []
        self.handle_inputs_thread = None
        self.last_command = None

        self.username = ''
        self.password = ''

        self.handle_event('welcome')
        self.handle_event('login_username')

        self.command_handlers = {
            'score': self.handle_score_command,
            'title': self.handle_title_command,
            'finger': self.handle_finger_command,
            'commands': self.handle_commands_command,
            'inventory': self.handle_inventory_command,
            'look': self.handle_look_command,
            'quit': self.handle_quit_command,
            'say': self.handle_say_command,
            'who': self.handle_who_command,
            'open': self.handle_open_command,
            'close': self.handle_close_command,
        }

        # create a thread that handles inputs on a loop
        self.handle_inputs_thread = thread.start_new_thread(
            self.handle_inputs,
            (),
        )

    def handle_inputs(self):
        """ Poll the connection for inputs and then sleep as long as needed """
        while self.connected:
            # default the delay to the bare minimum with no input
            delay = DELAYS['input_polling']

            if self.input_buffer:
                line = self.input_buffer.pop(0)[0]

                # default the delay to the bare minimum with input
                delay = DELAYS['__default__']

                # retrieve the delay from the command
                res = self.handle_input(line)
                if res is not None:
                    delay = int(res)

            # sleep as long as needed
            time.sleep(delay)

    def get_actor(self, fields=None):
        """
        Get a copy of the connection's actor
        If 'fields' is provided, the actor will also be freshly fetched from
            the database to ensure the proper data is returned
        """
        actor = self.actor.clone()
        if fields is not None:
            actor.fetch(fields=fields)
        return actor

    def destroy(self, quit=False):
        """ Remove this connection from the pool """
        self.connected = False
        self.server.destroy_connection(self, quit=quit)

    def listen(self):
        while self.connected:
            try:
                raw = self.socket.recv(4096)
                if len(raw) == 0:
                    return

                raw_lines = raw.strip().split("\n")
                self.input_buffer.append(raw_lines)
            except IOError:
                self.destroy()

    def handle_login_input(self, line):
        # check login
        if self.substate is 'username':
            self.username = line
            actor = Character(find={'name': line})

            if actor.exists():
                # get their password
                self.substate = 'password'
                self.handle_event('login_password')
            else:
                # create a new account? confirm with user
                # FIXME validate character name
                self.state = 'register'
                self.substate = 'confirm'
                self.handle_event('register_confirm', {
                    'name': self.username
                })
            return
        elif self.substate is 'password':
            self.password = line
            self.writeln("PASSWORD RECEIVED")
            self.writeln(self.username)
            self.writeln(self.password)

            actor = Character(find={'name': self.username, 'password': self.password})
            if actor.exists():
                self.actor = actor
                # FIXME if race and class have not yet been chosen, force them to customize
                self.state = 'motd'
                self.actor.self_check()
                self.handle_event('motd')
            else:
                self.destroy()

    def handle_register_input(self, line):
        if self.substate is 'confirm':
            if line.startswith('y'):
                self.substate = 'password'
                self.handle_event('register_password')
            elif line.startswith('n'):
                # FIXME handle asking username again, just the prompt
                self.state = 'login'
                self.substate = 'username'
                self.username = ''
                self.password = ''
                self.handle_event('login_username')
            else:
                # FIXME handle asking again VIA EVENT, just the prompt for confirm
                self.write('Is your name ' + self.username + '? ')
        elif self.substate is 'password':
            # FIXME validate password
            # FIXME use password hashing
            self.password = line
            character = Character(data={'name': self.username, 'password': self.password})
            # FIXME set starting room
            character.save()
            self.actor = character
            self.handle_event('motd')
            self.state = 'motd'
            # FIXME customize here
            #self.state = 'customize'
            #self.handle_event('customize')

    def handle_input(self, raw_line, store=True):
        line = raw_line.strip()
        line_parts = line.split(' ')
        command = line_parts[0].strip()

        if line is not '!' and store:
            self.last_command = line

        # FIXME constants for states
        if self.state is 'login':
            self.handle_login_input(line)
            return
        elif self.state is 'register':
            self.handle_register_input(line)
            return
        elif self.state is 'customize':
            self.handle_customize_input(line)
            return
        elif self.state is 'motd':
            self.state = 'playing'
            actor = self.get_actor()
            actor.self_check()
            actor.broadcast_to_room('connect')
            actor.look()
            return

        if line is '!':
            if not self.last_command:
                self.handle_event('input_invalid')
                return DELAYS['invalid_input']
            else:
                self.handle_input(self.last_command, store=False)
                return

        actor = self.get_actor()
        if actor:
            actor.fetch(fields=['name'])
            print(actor.name + '> ' + line)

        if not line:
            self.handle_event('input_invalid')
            return DELAYS['invalid_input']

        handler = None
        direction = None

        # direction walking?
        for direction_name in DIRECTIONS:
            if direction_name.startswith(command):
                direction = DIRECTIONS[direction_name]
                break

        # command mapper
        for name in self.command_handlers:
            if name.startswith(command):
                handler = self.command_handlers[name]
                break

        if direction:
            return self.handle_direction_command(direction)
        elif handler:
            return handler(line_parts[1:])
        else:
            self.handle_event('input_invalid')
            return DELAYS['invalid_input']

    def handle_direction_command(self, direction):
        actor = self.get_actor()
        return actor.walk(direction['id'])

    def handle_commands_command(self, args):
        self.writeln("A list of commands that are partially implemented:")
        commands = sorted(self.command_handlers.keys())
        count = 0
        for command in commands:
            count += 1
            self.write(command.ljust(20))

            if count % 4 is 0:
                self.writeln()

        if count % 4 is not 0:
            self.writeln()

    def handle_inventory_command(self, args):
        actor = self.get_actor()
        self.handle_event('inventory', {
            'items': actor.get_items()
        })

    def handle_look_command(self, args):
        actor = self.get_actor()
        actor.look()

    def handle_repeat_command(self, args):
        if not self.last_command:
            self.handle_event('input_invalid')
            return

        self.handle_input(self.last_command, store=False)

    def handle_title_command(self, args):
        # FIXME: limit the length of the title
        actor = self.get_actor()
        if not args:
            self.writeln("Change your title to what?")
            return

        actor.title = ' '.join(args).strip()
        actor.save()
        self.writeln("Ok.")

    def handle_finger_command(self, args):
        if not args:
            self.writeln("Finger who?")
            return

        search_name = args[0]
        actor = Character(find={'name': utils.get_proper_case(search_name)})

        if not actor.exists():
            self.writeln("No such player: {}.".format(utils.get_proper_case(search_name)))
            return

        actor.fetch(fields=['gender', 'name', 'title'])

        self.writeln("{B+{b-----------------------{C[ {BWa{bt{8e{wr{8d{be{Bep{RM{rUD {RCharacter Info {C]{b-----------------------{B+")
        self.writeln(" {{RN{{rame {}  {{x: {}  ( {} {{x)".format(actor.get_gender()['who_name'], actor.name, actor.title))
        self.writeln(" {RC{rlass   {x: gladiator                   {RC{rlan    {x: Order of the Radiant Heart")
        self.writeln(" {RR{race    {x: heucuva                     {RR{rank    {x:")
        self.writeln(" {RL{revel   {x: 101                         {RD{reity   {x: Bane")
        self.writeln(" {RA{rge     {x: 317                         {RA{rrena   {x: {c[{C1    {c] {RWins")
        self.writeln(" {RH{reight  {x: 6'0\"                                  {c[{C0    {c] {rLoss")
        self.writeln(" {RW{reight  {x: 210lbs                      {RH{rours   {x: 3255")
        self.writeln(" {RH{rair    {x: Wispy Black                 {RB{rirthday{x: Sat Jul 20 13:51:31 2013")
        self.writeln(" {RE{ryes    {x: Ice Blue                    {RO{rld {RC{rlan{x:")
        self.writeln(" {RPK {rrank {x: Veteran Foot Soldier        {RNPK {rrank{x:")
        self.writeln("{B+{b-------------------------------{B| {CDE{wSC{cRIP{wTI{CON {B|{b-------------------------------{B+")
        self.writeln("")
        if actor.description:
            for line in actor.description:
                self.writeln(actor.description)
        self.writeln("")
        self.writeln("{B+{b-----------------------------------------------------------------------------{B+")
        self.writeln(" {{C{} {{clast logged on at {{CSun Jun 22 13:14:31 2014".format(actor.name))
        self.writeln("")
        self.writeln(" {cLast on: {C2 {cdays {C10 {chours {C51 {cminutes ago.")
        self.writeln("{B+{b-----------------------------------------------------------------------------{B+{x")

    def handle_who_command(self, args):
        from mud import MUD
        mud = MUD()
        characters = mud.get_online_characters()

        from settings import CLASSES, RACES, GENDERS, GAME

        self.writeln("{G" + ("The Visible Mortals and Immortals of " + GAME['name']).center(80))
        self.writeln("{g-----------------------------------------------------------------------------")
        #self.writeln("CRE M Elmnt Wiz       [....NM....B] [AFK] Epsilon the Supremity of Death")
        #self.writeln("HRO M Human Mnk BlkCh   [.P......] Kwaichang The One Who Walks....")
        #self.writeln("HRO M Thken Mnk TrPwr   [.N......] Bootz")
        #self.writeln("HRO M H.Orc Mer KoB     [.N......] Demosthenes ok???? [Defender of Balance]")
        #self.writeln(" 79 M Elf   Sag RdHrt   [.P......] Madic Light in the Darkness (2563)")
        for character in characters:
            character.fetch(fields=[
                'name', 'who_name', 'gender', 'race', 'level', 'class',
                'title', 'bracket', 'clan', 'pkiller', 'bounty', 'mortal_admin',
                'immortal_questing', 'wanted', 'improved_invis', 'roleplaying',
                'spells',
            ])

            who_line = '{x'
            # TODO immortal levels
            who_line += str(character.level).rjust(3)
            who_line += ' '
            who_line += GENDERS[character.gender]['who_name']
            who_line += ' '
            if character.who_restring:
                who_line += character.who_restring
            else:
                if character.race_restring:
                    who_line += character.race_restring
                else:
                    who_line += RACES[character.race]['who_name']
                who_line += ' '
                if character.class_restring:
                    who_line += character.class_restring
                else:
                    who_line += CLASSES[character['class']]['who_name']
            who_line += ' '
            who_line += '{8Clans'  # clan here
            who_line += '   '
            who_line += '{x['

            # TODO LEADERSHIP ROLE OF CLAN, NYI
            who_line += '{W.'

            if character.pkiller:
                who_line += '{RP'
            else:
                who_line += '{BN'

            if character.mortal_admin:
                who_line += '{MM'
            else:
                who_line += '{W.'

            if character.roleplaying:
                who_line += '{WR'
            else:
                who_line += '{W.'

            if character.immortal_questing:
                who_line += '{BQ'
            else:
                who_line += '{W.'

            # FIXME use a proper lookup
            if character.improved_invis:
                who_line += '{WI'
            else:
                who_line += '{W.'

            if character.wanted:
                who_line += '{RW'
            else:
                who_line += '{W.'

            # display a bounty if they have one
            if character.bounty:
                who_line += '{CB'
            else:
                who_line += '{W.'

            who_line += '{x]'
            who_line += ' {x'
            if character.afk:
                who_line += '{x[{yAFK{x]'
                who_line += ' '
            who_line += character.name
            if character.title:
                who_line += ' '
                who_line += character.title

            if character.bracket:
                who_line += ' {x[' + character.bracket + '{x'

            self.writeln(who_line)

        self.writeln()
        self.writeln("{GPlayers found{g: {x" + str(len(characters)) + "   {GMost on today{g: {x-1")

    def handle_close_command(self, args):
        if not args[0]:
            self.writeln("Close what?")
            return

        # FIXME handle containers
        # FIXME write a function that returns what they're looking for, object, direction, player?

        actor = self.get_actor()
        actor.close_direction(args[0])

    def handle_open_command(self, args):
        if not args[0]:
            self.writeln("Open what?")
            return

        # FIXME handle containers
        # FIXME write a function that returns what they're looking for, object, direction, player?

        actor = self.get_actor()
        actor.open_direction(args[0])

    def handle_say_command(self, args):
        if not args:
            self.handle_event('say_what')
        else:
            self.actor.say(' '.join(args))

    def handle_score_command(self, args):
        actor = self.get_actor()
        actor.fetch()

        # TODO: actor.get_align_string()
        # TODO: actor.get_race
        # TODO: actor.get_class

        # TODO: actor.get_stat_total
        # TODO: actor.get_stat_total
        self.writeln("{{GName{{g: {{x{} {}".format(actor.name, actor.title))
        self.writeln("{GDesc{g: {xNeutral Good Male Elf Sage {GLevel{g:{x 79 {GAge{g:{x 76")
        self.writeln("{GHP{g:{x5125{g/{x5783 {g({c4298{g) {GMana{g:{x4389{g/{x4500 {g({c3420{g) {GMove{g:{x545{g/{x727 {g({c677{g) {GSaves{g:{x-24")
        self.writeln("{g------------+-----------------------+---------------")
        self.writeln("{GStr{g: {x12{g({x20{g) | {GItems{g:{x     34{g/{x156     {g| {GHitroll{g: {x133")
        self.writeln("{GInt{g: {x14{g({x19{g) | {GWeight{g:{x   168{g/{x447     {g| {GDamroll{g: {x133")
        self.writeln("{GWis{g: {x13{g({x21{g) +----------------+------+---------------")
        self.writeln("{GDex{g: {x15{g({x21{g) | {GPractices{g:{x 328 {g| {GArena Wins{g:{x     0")
        self.writeln("{GCon{g: {x11{g({x19{g) | {GTrains{g:{x      0 {g| {GArena Losses{g:{x   0")
        self.writeln("{g------------+---------+------+----------------------")
        self.writeln("{GCoins Platinum{g:{x   908 {g|{G Experience")
        self.writeln("      {GGold{g:{x        12 {g|{G  Current{g:{x 232993")
        self.writeln("      {GSilver{g:{x      44 {g|{G  Needed{g:{x  2607")
        self.writeln("{g----------------------+-----------------------------")
        self.writeln("{GArmor Pierce{g:{x   -90 [{Bsuperbly armored{x]")
        self.writeln("{GBash{g:{x     -91 [{Bsuperbly armored{x]")
        self.writeln("{GSlash{g:{x    -90 [{Bsuperbly armored{x]")
        self.writeln("{GMagic{g:{x    -73 [{Bsuperbly armored{x]")
        self.writeln("{g----------------+-------------+---------------------")
        self.writeln("{GAlignment{g:{x 1000 {g| {GWimpy{g:{x    0 {g| {GQuest Points{g:{x 2007")
        self.writeln("{g----------------+-------------+---------------------")
        self.writeln("{GPkStatus{g:{x PK")
        self.writeln("{g---------------------------------------------------- ")

    def handle_quit_command(self, args):
        # TODO check you're allowed to quit right now
        self.writeln("{RYou feel a hand grab you, you begin to fly upwards!");
        self.writeln("{BYou pass through the clouds and out of the world!");
        self.writeln("{GYou have rejoined Reality!");
        self.writeln();
        self.writeln("{WFor {RNews, {CRoleplaying {Wand {MInfo, {WVisit our website!");
        self.writeln("{CH{cttp://www{W.{CW{caterdeep{CM{cud{W.{CCom{x");
        self.flush()
        self.destroy(quit=True)

    def handle_event(self, event, data=None):
        """ Handle the outputs from the game """

        print('EVENT FIRED: ' + repr(event) + ' ' + repr(data))
        if not data:
            data = {}

        method = 'handle_{}_event'.format(event)

        if hasattr(self, method):
            func = getattr(self, method)
            func(data)
        else:
            msg = "*** Unhandled event: {}".format(repr(event))
            self.writeln(msg)
            print(msg)


    def handle_register_confirm_event(self, data):
        self.writeln("""
+------------------------[ Welcome to Waterdeep ]-------------------------+

  We are a roleplaying -encouraged- mud, meaning roleplaying is not
  required by our players but we ask that non-roleplayers abide to a few
  rules and regulations.  All names are to be roleplayish in nature and
  this policy is enforced by the staff.

    1. Do not use names such as Joe, Bob, Larry, Carl and so forth.
    2. Names of Forgotten Realms Deities are reserved for staff members.
    3. Do not use combo word names such as Blackbeard or Rockdeath, etc.

  If we find your name is not suitable for our environment, an immortal
  staff member will appear before you and offer you a rename.  Please be
  nice and civil, and we will return with the same.

+--------------[ This MUD is rated R for Mature Audiences ]---------------+""")
        self.writeln()
        self.write("Did I get that right, " + data['name'] + '? ')

    def handle_register_password_event(self, data):
        # FIXME: verify this logic is correct
        self.write("Please choose a password: ")

    def handle_welcome_event(self, data):
        self.write("""


            ;::::;
           ;::::; :;
         ;:::::'   :;
        ;:::::;     ;.
       ,:::::'       ;           OOO\\
       ::::::;       ;          OOOOO\\
       ;:::::;       ;         OOOOOOOO
      ,;::::::;     ;'         / OOOOOOO
    ;:::::::::`. ,,,;.        /  / DOOOOOO
  .';:::::::::::::::::;,     /  /    DOOOO
 ,::::::;::::::;;;;::::;,   /  /       DOOO      WDM 2.0 Code by Waterdeep
;`::::::`'::::::;;;::::: ,#/  /        DOOO     Mud Entertainment. (c) 2007
:`:::::::`;::::::;;::: ;::#  /          DOOO
::`:::::::`;:::::::: ;::::# /            DOO   Undermountain 1.0 Engine by
`:`:::::::`;:::::: ;::::::#/             DOO   Waterdeep MUD Entertainment
 :::`:::::::`;; ;:::::::::##              OO           (c) 2014
 ::::`:::::::`;::::::::;:::#              OO
 `:::::`::::::::::::;'`:;::#              O  Owned & Operated by Nisstyre
  `:::::`::::::::;' /  / `:#                 E-Mail:  WDMUD@WaterdeepMUD.Com
   ::::::`:::::;'  /  /   `#
           ##    ##  ####  ###### ######  ####  ######  ###### ###### #####
           ##    ## ##  ##   ##   ##     ##  ##  ##  ## ##     ##     ##  ##
           ## ## ## ######   ##   ####   ##  ##  ##  ## ####   ####   #####
           ## ## ## ##  ##   ##   ##     #####   ##  ## ##     ##     ##
            ##  ##  ##  ##   ##   ###### ##  ## ######  ###### ###### ##
                          C I T Y  O F  S P L E N D O R S
                                   [ Est 1997 ]

""")

    def handle_login_password_event(self, data):
        self.write("Password: ")

    def handle_login_username_event(self, data):
        self.write("Why have you come....go away or choose a name: ")

    def handle_connect_event(self, data):
        actor = data['actor']
        me = self.get_actor()
        if not actor.equals(me):
            actor.fetch(fields=['name'])
            template = "{{r*{{R*{{r*{{W {} has connected{{x"
            self.writeln(template.format(actor.name))

    def handle_disconnect_event(self, data):
        actor = data['actor']
        me = self.get_actor()
        if not actor.equals(me):
            actor.fetch(fields=['name'])
            template = "{{r*{{R*{{r*{{W {} has disconnected{{x"
            self.writeln(template.format(actor.name))

    def handle_inventory_event(self, data):
        items = data['items']
        self.writeln("You are carrying:")
        if items and len(items) > 0:
            for item in items:
                self.writeln("     {}".format(item['name']))
        else:
                self.writeln("     {}".format('Nothing'))

    def handle_look_room_event(self, data):
        room = data['room']
        self.writeln("{B" + room.name + "{x")

        opened_exit_names = []
        closed_exit_names = []

        for direction_key in DIRECTIONS:
            direction = DIRECTIONS[direction_key]
            exit = room.get_exit(direction['id'])
            if exit:
                colored_name = direction['colored_name']
                if 'closed' in exit and exit['closed']:
                    closed_exit_names.append(colored_name)
                else:
                    opened_exit_names.append(colored_name)

        if not opened_exit_names:
            opened_exit_string = '{xnone'
        else:
            opened_exit_string = ' '.join(opened_exit_names)

        if not closed_exit_names:
            closed_exit_string = '{xnone'
        else:
            closed_exit_string = ' '.join(closed_exit_names)

        count = 0
        for line in room.description:
            count += 1
            if count is 1:
                self.write('  {x')  # indent first line a bit
            self.writeln(line)
        self.writeln()
        self.writeln("{x[{GExits{g: " + opened_exit_string + "]   [{GDoors{g: " + closed_exit_string + "]")

        for item in room.get_items():
            if 'room_name' in item:
                self.writeln('     ' + item['room_name'])

        me = self.get_actor()

        for actor in room.get_actors():
            # do not show yourself
            if actor.equals(me):
                continue

            actor.fetch(fields=['name'])
            if actor.is_character():
                self.write(actor.name)
                if actor.title:
                    self.write(' ' + actor.title)
                self.writeln(' {xis here.')
            else:
                self.writeln(actor.room_name)

    def handle_motd_event(self, data):
        from settings import GAME
        print("HANDLING MOTD")
        # FIXME make this load from a text file
        self.writeln("""
                                                          __
                                                        //  \\\\
                                                       // /\ \\\\
                                                       \\\\ \/ //
                                                        \\\\__//
                                                        [|//|]
                                                        [|//|]
               Welcome to                               [|//|]
                                                        [|//|]
            W A T E R D E E P                           [|//|]
                                                        [|//|]
            City of Splendors                /)         [|//|]        (\\
                                            //\_________[|//|]________/\\\\
                est. 1997                   ))__________||__||_________((
                                           <_/         [  \/  ]        \_>
                                                       ||    ||
                                                       ||    ||
                                                       ||    ||
 [x] Waterdeep is rated [R] for Mature Audiences Only. ||    ||
 [x] Please follow the rules of the game. [Help Rules] ||    ||
 [x] Check the News Board for game info.               ||    ||
                                                       ||    ||
 [x] Type HELP for our directory of help files.        ||    ||
 [x] Type HELP NEWBIE for basic directions and help.   ||    ||
                                                       ||    ||
                                                       ||    ||
                                                       ||    ||
         Waterdeep Entertainment                       ||    ||
            www.waterdeep.org                          ||    ||
                                                       ||    ||
                                                       ||    ||
                                                       ||    ||
                                                       \\\\    //
                                                        \\\\  //
                                                         \\\\//
                                                          \/
[Hit Enter to Continue]""")
        self.writeln("Welcome to the " + GAME['name'] + " " + GAME['version'] + " test server")
        self.writeln("For a list of (semi-)working commands, type 'commands'")
        self.writeln()

    def handle_say_event(self, data):
        me = self.get_actor()
        actor = data['actor']
        actor.fetch(fields=['name'])
        if actor.equals(me):
            self.writeln("{{MYou say {{x'{{m{}{{x'".format(data['message']))
        else:
            self.writeln("{{M{} says {{x'{{m{}{{x'".format(data['actor'].name, data['message']))

    def handle_walk_invalid_event(self, data):
        self.writeln("Alas, you cannot go that way.")

    def handle_walk_enter_event(self, data):
        me = self.get_actor()
        actor = data['actor']
        actor.fetch(fields=['name'])

        if not actor.equals(me):
            self.writeln("{} has arrived.".format(actor.name))

    def handle_open_direction_already_open_event(self, data):
        self.writeln("It's already open.")

    def handle_open_direction_not_found_event(self, data):
        self.writeln("I see no door {} here.".format(data['direction']))

    def handle_input_invalid_event(self, data):
        self.writeln("Huh?")

    def handle_walk_leave_event(self, data):
        me = self.get_actor()
        actor = data['actor']
        actor.fetch(fields=['name'])
        direction = data['direction']
        # FIXME verify this text is correct

        if not actor.equals(me):
            self.writeln("{} leaves {}{{x.".format(actor.name, direction['colored_name']))

    def write(self, message):
        self.buffer += utils.parse_colors(message)

    def writeln(self, message=''):
        self.write(message + "\n")

    def write_prompt(self):
        # FIXME use constant
        # do not display prompt if you're not actually playing yet
        if self.state is not 'playing':
            return

        # if you don't have a player yet, you can't see your prompt either
        actor = self.get_actor()
        actor.fetch(fields=['room_vnum'])
        print(actor.name)
        if not actor or not actor.exists():
            return

        self.writeln()
        room = actor.get_room()

#|      %h : Displays your current hit points.                        |
#|      %H : Displays your maxiumum hit points.                       |
#|      %g : Displays your hps color coded.  (green, yellow, red)     |
#|      %m : Displays your current mana points.                       |
#|      %M : Displays your maxiumum mana points.                      |
#|      %v : Displays your current moves left.                        |
#|      %V : Displays your maximum movements.                         |
#|      %x : Displays your current experience.                        |
#|      %X : Displays your experience to level.                       |
#|      %a : Displays your alignment.                                 |
#|      %r : Displays the room you're in.                             |
#|      %e : Displays the exits from the room in NESWDU style.        |
#|      %c : Displays a carriage return. (multi-line prompts)         |
#|      %R : Displays the vnum you are in (Immortal Only).            |
#|      %z : Displays the area name you are in (Immortal Only).       |
#|      %q : Displays the time until you may quest again.             |
#|      %Q : Displays the time in your current quest.                 |
#|      %t : Displays the current mud time.                           |
#|                                                                    |

        actor.fetch(fields=['name', 'stats', 'room_vnum'])
        template = "{8[{R%h{8/{r%H{8h {B%m{8/{B%M{8m {M%v{8v {W%N{8({Y%x{8) {W%r{8({w%q{8/{w%t{8) {W%a{8] "
        template = template.replace('%N', actor.name)
        template = template.replace('%r', room.name)

        template = template.replace('%h', str(0))
        template = template.replace('%H', str(0))
        template = template.replace('%m', str(0))
        template = template.replace('%M', str(0))
        template = template.replace('%v', str(0))
        template = template.replace('%V', str(0))
        template = template.replace('%x', str(3000))
        template = template.replace('%X', str(3000))
        template = template.replace('%t', '{w12{Bam{x')
        template = template.replace('%q', str(0))
        template = template.replace('%a', str(0))
        self.write(template + '{x')

    def flush(self):
        if self.buffer:
            print('flush')
            self.write_prompt()
            try:
                self.socket.sendall(self.buffer)
            except Exception:
                self.destroy()
            self.buffer = ''
