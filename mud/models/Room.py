from Entity import Entity


class Room(Entity):
    collection = 'rooms'

    def get_actors(self):
        all_actors = []
        from mud import MUD
        mud = MUD()

        from Actor import Actor
        actors = Actor().query_by_room(room=self)
        for actor in actors:
            all_actors.append(actor)

        characters = mud.get_online_characters(room_vnum=self.vnum)
        for character in characters:
            all_actors.append(character)

        return all_actors

    def broadcast_to_room(self, event, data):
        actors = self.get_actors()
        for actor in actors:
            actor.broadcast(event, data)

    def get_items(self):
        items = self.items
        if not items:
            return []
        return items

    def get_exit(self, direction):
        if not self.exits:
            return self.exits

        if not direction in self.exits:
            return None

        return self.exits[direction]

    def direction_exists(self, direction):
        exit = self.get_exit(direction)
        return exit is not None

    def direction_is_door(self, direction):
        exit = self.get_exit(direction)
        return exit and 'door' in exit and exit['door']

    def direction_is_open(self, direction):
        if not self.direction_is_door(direction):
            return False

        exit = self.get_exit(direction)
        return exit and 'closed' in exit and not exit['closed']

    def direction_is_closed(self, direction):
        return not self.direction_is_open(direction)

    def open_direction(self, direction):
        self.fetch(fields=['exits'])
        self.exits[direction]['closed'] = False
        self.save()

    def close_direction(self, direction):
        self.fetch(fields=['exits'])
        self.exits[direction]['closed'] = True
        self.save()
