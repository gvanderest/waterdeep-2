from mud import DatabaseConnection


class Entity(object):
    _database = DatabaseConnection()

    def __init__(self, data=None, find=None, fields=None):
        self._data = data

        if not data:
            data = {}

        self._data = data

        if find:
            self.fetch(params=find, fields=fields)

    def clone(self):
        return self.__class__(data=self._data)

    def equals(self, other):
        if not other._id or not self._id:
            raise Exception("Entity compare failed because one of the objects is missing an _id")
        return str(other._id) == str(self._id)

    def __setattr__(self, attr, val):
        if attr is '_data':
            super(Entity, self).__setattr__('_data', val)
            return super(Entity, self).__getattribute__('_data')

        self._data[attr] = val
        return self._data[attr]

    def __getattr__(self, attr):
        if attr is '_data':
            return super(Entity, self).__getattribute__('_data')

        if not attr in self._data:
            return None

        return self._data[attr]

    def __getitem__(self, attr):
        return self.__getattr__(attr)

    def __setitem__(self, attr, value):
        return self.__setattr__(attr, value)

    def get_mud(self):
        from mud import MUD
        return MUD()

    def _get_collection(self):
        coll = self._database[self.collection]
        return coll

    def save(self):
        if not self.collection:
            raise Exception("A collection must be defined for {} entity"
                            .format(type(self)))

        coll = self._get_collection()
        if self.exists():
            coll.update({'_id': self._id}, {'$set': self._data})
        else:
            self._id = coll.save(self._data)

    def exists(self):
        if not self._data or not '_id' in self._data:
            return False
        return True

    def fetch(self, params=None, fields=None):
        if not params:
            if self._id:
                params = self._id
            else:
                raise Exception("No search parameter provided, and ID for Entity not found when trying to fetch")

        coll = self._database[self.collection]

        res = coll.find_one(params, fields=fields, await_data=False)
        if not res:
            res = {}

        super(Entity, self).__setattr__('_data', res)
