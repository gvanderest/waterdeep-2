from Entity import Entity
from Room import Room
from settings import DIRECTIONS, DELAYS, GENDERS


class Actor(Entity):
    collection = 'actors'

    def get_room(self):
        # FIXME ensure updated database fetch
        self.fetch(fields=['room_vnum'])
        room = Room(find={'vnum': self.room_vnum})
        if not room.exists():
            room = Room()
            room.saveable = False
            room.name = 'The Void'
            room.description = [
                'You are floating in nothingness.'
            ]
        return room

    def is_character(self):
        return False

    def look(self):
        room = self.get_room()
        self.broadcast('look_room', {
            'room': room
        })

    def say(self, message):
        self.broadcast_to_room('say', {
            'message': message,
        })

    def get_connections(self):
        print('fetching actor connections')
        from mud import MUD
        mud = MUD()
        connections = mud.get_connections_by_actor(self)
        return connections

    def get_items(self):
        self.fetch(fields=['items'])
        return self.items

    def get_gender(self):
        # FIXME: use a self.fetch_additional(fields=['gender'])?
        if not self.gender in GENDERS:
            return None

        return GENDERS[self.gender]

    def broadcast(self, event, data=None):
        if not data:
            data = {}

        # find the connection for this actor, and pass event to each client
        actor_connections = self.get_connections()
        for connection in actor_connections:
            connection.handle_event(event, data)

    def broadcast_to_room(self, event, data=None):
        if not data:
            data = {}

        data['actor'] = self

        r = self.get_room()
        r.broadcast_to_room(event, data)

    def query_by_room(self, room):
        actors = []

        coll = self._get_collection()
        results = coll.find({'room_vnum': room.vnum}, fields=['_id'])
        print("ACTORS FOUND")
        print(repr(results))

        for record in results:
            actors.append(Actor(data=record))

        return actors

    def self_check(self):
        self.fetch()

        if not self.level:
            self.level = 1

        if not self.title:
            self.title = ''

        # FIXME: constants
        if not self['class']:
            self['class'] = 'vampire'

        if not self.race:
            self.race = 'human'

        if not self.gender:
            self.gender = 'none'

        if not self.stats:
            self.stats = {}

        if not self.room_vnum:
            self.room_vnum = 1

        if not self.items:
            self.items = []

        if not 'pkiller' in self._data:
            self.pkiller = False

        if not 'bounty' in self._data:
            self.bounty = 0

        if not 'wanted' in self._data:
            self.wanted = False

        if not 'mortal_admin' in self._data:
            self.mortal_admin = False

        if not 'immortal_questing' in self._data:
            self.immortal_questing = False

        if not 'roleplaying' in self._data:
            self.roleplaying = False

        if not self.equipment:
            self.equipment = {}

        print('AFTER: ' + repr(self._data))

        self.save()

    def open_direction(self, direction):
        self.fetch(fields=['room_vnum'])
        room = self.get_room()

        data = {
            'direction': direction
        }

        # TODO check the actor can see the door
        if room.direction_exists(direction):
            if room.direction_is_door(direction):
                if not room.direction_is_open(direction):
                    room.open_direction(direction)
                    self.broadcast('open_direction', data)
                else:
                    self.broadcast('open_direction_already_open', data)
            else:
                self.broadcast('open_direction_not_door', data)
                return
        else:
            self.broadcast('open_direction_not_found', data)
            return

    def walk(self, direction):
        self.fetch(fields=['room_vnum'])
        room = self.get_room()
        exit = room.get_exit(direction)
        if not exit:
            self.broadcast('walk_invalid')
            return DELAYS['invalid_walk']

        # FIXME check in combat

        if 'door' in exit and exit['door']:
            if 'closed' in exit and exit['closed']:
                # FIXME check pass door and stuff
                self.broadcast('walk_door_closed', {
                    'exit': exit
                })
                return DELAYS['invalid_walk']

        self.broadcast_to_room('walk_leave', {
            'direction': DIRECTIONS[direction]
        })
        self.room_vnum = exit['vnum']
        self.save()
        opposite = DIRECTIONS[direction]['opposite']
        self.broadcast_to_room('walk_enter', {
            'direction': opposite
        })

        self.look()
