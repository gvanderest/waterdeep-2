EXPORT = {
    'collections': [
        'area_definitions',
        'item_definitions',
        'room_definitions',
        'actor_definitions',
        'characters'
    ]
}
