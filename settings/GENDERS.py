GENDERS = {
    'male': {
        'name': 'Male',
        'who_name': '{BM',
    },
    'female': {
        'name': 'Female',
        'who_name': '{MF',
    },
    'none': {
        'name': 'None',
        'who_name': '{8N',
    }
}

for GENDER in GENDERS:
    GENDERS[GENDER]['id'] = GENDER
