# DELAYS
# in seconds
DELAYS = {
    '__default__': 0.5,

    # player commands
    'invalid_input': 0.2,
    'invalid_walk': 0.2,

    # server-side
    'input_polling': 0.05,
    'socket_flush': 0.1,
}
