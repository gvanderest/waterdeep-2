DIRECTIONS = {
    'north': {
        'name': 'North',
        'colored_name': '{Rnorth{x',
        'opposite': 'south',
    },
    'east': {
        'name': 'East',
        'colored_name': '{Meast{x',
        'opposite': 'west',
    },
    'south': {
        'name': 'South',
        'colored_name': '{rsouth{x',
        'opposite': 'north',
    },
    'west': {
        'name': 'West',
        'colored_name': '{mwest{x',
        'opposite': 'east',
    },
    'up': {
        'name': 'Up',
        'colored_name': '{Yup{x',
        'opposite': 'down',
    },
    'down': {
        'name': 'Down',
        'colored_name': '{ydown{x',
        'opposite': 'up',
    },
}

for DIRECTION in DIRECTIONS:
    DIRECTIONS[DIRECTION]['id'] = DIRECTION
