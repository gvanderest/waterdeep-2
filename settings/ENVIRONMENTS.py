import sys

ENVIRONMENTS = {
    'live': {
        'port': 4200,
        'name': 'Waterdeep Live Port',
        'database_uri': 'mongodb://localhost:4201/waterdeep',
    },
    'builder': {
        'port': 4202,
        'name': 'Waterdeep Builder Port',
        'database_uri': 'mongodb://localhost:4201/waterdeep-builder',
    }
}


def get_environment(environment_name=None):
    if not environment_name:
        if len(sys.argv) > 1:
            environment_name = sys.argv[1]

    if not environment_name in ENVIRONMENTS:
        print("Invalid environment name '{}' provided".format(environment_name))
        print("See the settings/ENVIRONMENTS.py file for defined environments")
        sys.exit(0)

    return ENVIRONMENTS[environment_name]
