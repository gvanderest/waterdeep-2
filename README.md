Waterdeep Python Engine
=======================

Introduction
------------
Welcome to the beginnings of the codebase for the Waterdeep 3.0(?) engine.

Technologies
------------
- Python 2.7 Scripting Language
- MongoDB 2.6 Document Storage

Goals
-----
- Recreate what Waterdeep offers in terms of playability and expected outcomes
- Make the code much easier to modify and traverse to add new functionality
- Provide a smaller RAM and CPU footprint for more inexpensive hosting solutions
- Use multiple threads to provide concurrency to systems of the game