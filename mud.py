from mud import MUD
import sys

if len(sys.argv) < 2:
    print("You must provide the environment name.")
    print("python mud.py <environment>")
    print()
    print("Environments are defined in the settings/ENVIRONMENTS.py file")
    sys.exit(0)

ENVIRONMENT_NAME = sys.argv[1]

mud = MUD()
mud.run()
